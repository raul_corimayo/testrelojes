<?php

include_once './EstadoReloj.php';

abstract class Reloj {
    
    private $anterior;
    private $actual;
    
    
    function __construct() {
        $this->anterior = new EstadoReloj();
        $this->actual = new EstadoReloj();
    }
    
    
    public function costo(){
        return $this->actual->costo($this->anterior);
    }
    
    public function getGastoEnergetico($segundos){
        $this->reset();
        $gasto = 0;
        for ($i = 0; $i <= $segundos; $i++) {
            $this->setValor($i);
            $gasto += $this->costo();
            //echo '<br/> i '.$i.' gasto = '.$gasto;
        }
        return $gasto;
    }
    
    public abstract function setValor($valor);
    
    public function reset(){
        $this->actual->setValor();
        $this->anterior->setValor();
    }

    
    public function getActual(){
        return $this->actual;
    }
    
    public function getAnterior(){
        return $this->anterior;
    }
    
}
