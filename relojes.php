<?php

class Digito {
    
    
    const OFF = -1;
    private $valor;
    private $izquierda_arriba = 0;
    private $izquierda_abajo = 0;
    private $arriba = 0;
    private $medio = 0;
    private $abajo = 0;
    private $derecha_arriba = 0;
    private $derecha_abajo = 0;
    
    
    public function setValor($valor = self::OFF){
        $this->valor = $valor;
        switch ($valor) {
            case 0: $this->setSegmentos(1, 1, 1, 0, 1, 1, 1);break;
            case 1: $this->setSegmentos(0, 0, 0, 0, 0, 1, 1);break;
            case 2: $this->setSegmentos(0, 1, 1, 1, 1, 1, 0);break;
            case 3: $this->setSegmentos(0, 0, 1, 1, 1, 1, 1);break;
            case 4: $this->setSegmentos(1, 0, 0, 1, 0, 1, 1);break;
            case 5: $this->setSegmentos(1, 0, 1, 1, 1, 0, 1);break;
            case 6: $this->setSegmentos(1, 1, 1, 1, 1, 0, 1);break;
            case 7: $this->setSegmentos(0, 0, 1, 0, 0, 1, 1);break;
            case 8: $this->setSegmentos(1, 1, 1, 1, 1, 1, 1);break;
            case 9: $this->setSegmentos(1, 0, 1, 1, 1, 1, 1);break;
            case self::OFF: $this->setSegmentos(0, 0, 0, 0, 0, 0, 0);break;
            default://error        
        }
    }
    
    private function setSegmentos($izquierda_arriba, $izquierda_abajo, $arriba, 
            $medio, $abajo, $derecha_arriba, $derecha_abajo)
    {
        $this->izquierda_arriba = $izquierda_arriba;
        $this->izquierda_abajo = $izquierda_abajo;
        $this->arriba = $arriba;
        $this->medio = $medio;
        $this->abajo = $abajo;
        $this->derecha_arriba = $derecha_arriba;
        $this->derecha_abajo = $derecha_abajo;
    }
    
    
    public function __toString() {
        return "$this->izquierda_arriba, $this->izquierda_abajo, $this->arriba, "
                . "$this->medio, $this->abajo, $this->derecha_arriba, "
                . "$this->derecha_abajo";
    }
    
    public function costo(Digito $anterior){
        //echo "<br/> actual $this, anterior $anterior";
        
        // Solo al encender tiene un costo. 
        // Para eso el estado anterior debe estar apagado y el catual encendido.
        $costo = 0;
        
        if( ! $anterior->izquierda_arriba  &&  $this->izquierda_arriba){
            $costo++;
        }
        
        if( ! $anterior->izquierda_abajo  &&  $this->izquierda_abajo){
            $costo++;
        }
        
        if( ! $anterior->arriba  &&  $this->arriba){
            $costo++;
        }
        
        if( ! $anterior->medio  &&  $this->medio){
            $costo++;
        }
        
        if( ! $anterior->abajo  &&  $this->abajo){
            $costo++;
        }
        
        if( ! $anterior->derecha_arriba  &&  $this->derecha_arriba){
            $costo++;
        }
        
        if( ! $anterior->derecha_abajo  &&  $this->derecha_abajo){
            $costo++;
        }
        
        return $costo;
    }
}


class ParDigito {
    
    const OFF = -1;
    private $valor;
    private $decenas;
    private $unidades;
    
    
    function __construct($valor = self::OFF) {
        $this->decenas = new Digito();
        $this->unidades = new Digito();
        $this->setValor($valor);
    }

    public function setValor($valor = self::OFF){
        $this->valor = $valor;
        if($valor == self::OFF){
            $this->decenas->setValor(self::OFF);
            $this->unidades->setValor(self::OFF);
        }else{
            $decenas = $this->getDecenas($valor);
            $unidades = $this->getUnidades($valor);
            $this->decenas->setValor($decenas);
            $this->unidades->setValor($unidades);
        }
    }
    
    private function getDecenas($valor){
        return floor($valor/10);
    }
    
    private function getUnidades($valor){
        return $valor - $this->getDecenas($valor) * 10;
    }
    
    
    public function costo(ParDigito $anterior){
        $costo = 0;
        $costo += $this->decenas->costo($anterior->decenas);
        $costo += $this->unidades->costo($anterior->unidades);
        return $costo;
    }
    
    public function getValor(){
        return $this->valor;
    }
}


class EstadoReloj {
    
    const OFF = -1;
    private $valor;// tiempo total expresado en segundos
    private $hora;
    private $minuto;
    private $segundo;
    
    
    function __construct($valor = self::OFF) {
        $this->hora = new ParDigito();
        $this->minuto = new ParDigito();
        $this->segundo = new ParDigito();
        $this->setValor($valor);
    }

    
    public function setValor($valor = self::OFF){
        $this->valor = $valor;
        if($valor == self::OFF){
            $this->hora->setValor(ParDigito::OFF);
            $this->minuto->setValor(ParDigito::OFF);
            $this->segundo->setValor(ParDigito::OFF);
        }else{
            // obtener horas, minutos y segundos
            $horas = floor($valor / 3600);
            $resto_segundos_ms = $valor - $horas * 3600;
            $minutos = floor($resto_segundos_ms / 60);
            $resto_segundos_s = $resto_segundos_ms - $minutos * 60;
            $this->hora->setValor($horas);
            $this->minuto->setValor($minutos);
            $this->segundo->setValor($resto_segundos_s);
        }
    }
    
    public function getValor(){
        return $this->valor;
    }
    
    public function costo(EstadoReloj $anterior){
        $costo = 0;
        $costo += $this->hora->costo($anterior->hora);
        $costo += $this->minuto->costo($anterior->minuto);
        $costo += $this->segundo->costo($anterior->segundo);
        return $costo;
    }
    
}


abstract class Reloj {
    
    private $anterior;
    private $actual;
    
    
    function __construct() {
        $this->anterior = new EstadoReloj();
        $this->actual = new EstadoReloj();
    }
    
    
    public function costo(){
        return $this->actual->costo($this->anterior);
    }
    
    public function getGastoEnergetico($segundos){
        $this->reset();
        $gasto = 0;
        for ($i = 0; $i <= $segundos; $i++) {
            $this->setValor($i);
            $gasto += $this->costo();
            //echo '<br/> i '.$i.' gasto = '.$gasto;
        }
        return $gasto;
    }
    
    public abstract function setValor($valor);
    
    public function reset(){
        $this->actual->setValor();
        $this->anterior->setValor();
    }

    
    public function getActual(){
        return $this->actual;
    }
    
    public function getAnterior(){
        return $this->anterior;
    }
    
}


class RelojEstandar extends Reloj{
    
    public function setValor($valor) {
        $this->getActual()->setValor($valor);
    }

}


class RelojPremium extends Reloj{
    
    public function setValor($valor) {
        $this->getAnterior()->setValor($this->getActual()->getValor());
        $this->getActual()->setValor($valor);
    }

}


// Casos de Prueba
$relojEstandar = new RelojEstandar();
$resultado     = $relojEstandar->getGastoEnergetico(0);
echo 'Reloj Estandar  (0seg)     : ' . $resultado . "\n";
$resultado = $relojEstandar->getGastoEnergetico(4);
echo 'Reloj Estandar (4seg)      : ' . $resultado . "\n";

$relojPremium = new RelojPremium();
$resultado    = $relojPremium->getGastoEnergetico(0);
echo 'Reloj Premium  (0seg)      : ' . $resultado . "\n";
$resultado = $relojPremium->getGastoEnergetico(4);
echo 'Reloj Premium (4seg)       : ' . $resultado . "\n";

// Completar con resolucion de punto 2
$segundosDia = 24 * 60 * 60;
$gastoEstandar = $relojEstandar->getGastoEnergetico($segundosDia);
$gastoPremium  = $relojPremium->getGastoEnergetico($segundosDia);
$ahorro = $gastoEstandar - $gastoPremium;
echo 'Ahorro Premium vs Estandar : ' . $ahorro . "\n";
