<?php

include_once './Reloj.php';

class RelojPremium extends Reloj{
    
    public function setValor($valor) {
        $this->getAnterior()->setValor($this->getActual()->getValor());
        $this->getActual()->setValor($valor);
    }

}
