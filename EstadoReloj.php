<?php

include_once './ParDigito.php';

class EstadoReloj {
    
    const OFF = -1;
    private $valor;// tiempo total expresado en segundos
    private $hora;
    private $minuto;
    private $segundo;
    
    
    function __construct($valor = self::OFF) {
        $this->hora = new ParDigito();
        $this->minuto = new ParDigito();
        $this->segundo = new ParDigito();
        $this->setValor($valor);
    }

    
    public function setValor($valor = self::OFF){
        $this->valor = $valor;
        if($valor == self::OFF){
            $this->hora->setValor(ParDigito::OFF);
            $this->minuto->setValor(ParDigito::OFF);
            $this->segundo->setValor(ParDigito::OFF);
        }else{
            // obtener horas, minutos y segundos
            $horas = floor($valor / 3600);
            $resto_segundos_ms = $valor - $horas * 3600;
            $minutos = floor($resto_segundos_ms / 60);
            $resto_segundos_s = $resto_segundos_ms - $minutos * 60;
            $this->hora->setValor($horas);
            $this->minuto->setValor($minutos);
            $this->segundo->setValor($resto_segundos_s);
        }
    }
    
    public function getValor(){
        return $this->valor;
    }
    
    public function costo(EstadoReloj $anterior){
        $costo = 0;
        $costo += $this->hora->costo($anterior->hora);
        $costo += $this->minuto->costo($anterior->minuto);
        $costo += $this->segundo->costo($anterior->segundo);
        return $costo;
    }
    
}
