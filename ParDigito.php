<?php

include_once './Digito.php';

class ParDigito {
    
    const OFF = -1;
    private $valor;
    private $decenas;
    private $unidades;
    
    
    function __construct($valor = self::OFF) {
        $this->decenas = new Digito();
        $this->unidades = new Digito();
        $this->setValor($valor);
    }

    public function setValor($valor = self::OFF){
        $this->valor = $valor;
        if($valor == self::OFF){
            $this->decenas->setValor(self::OFF);
            $this->unidades->setValor(self::OFF);
        }else{
            $decenas = $this->getDecenas($valor);
            $unidades = $this->getUnidades($valor);
            $this->decenas->setValor($decenas);
            $this->unidades->setValor($unidades);
        }
    }
    
    private function getDecenas($valor){
        return floor($valor/10);
    }
    
    private function getUnidades($valor){
        return $valor - $this->getDecenas($valor) * 10;
    }
    
    
    public function costo(ParDigito $anterior){
        $costo = 0;
        $costo += $this->decenas->costo($anterior->decenas);
        $costo += $this->unidades->costo($anterior->unidades);
        return $costo;
    }
    
    public function getValor(){
        return $this->valor;
    }
}
